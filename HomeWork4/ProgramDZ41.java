import java.util.Scanner;

class ProgramDZ41{
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter num: ");
        int n = in.nextInt();

        if((n > 0) && ((n & (n - 1)) == 0))
            System.out.println("YES");
        else
            System.out.println("NO");
    }
    
}