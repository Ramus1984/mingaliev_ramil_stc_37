class ProgramDZ42By {//бинарный поиск

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberForSearch = scanner.nextInt();

        int[] number = {1, 7, 22, 28, 31, 33, 43, 55, 87, 99};

        int left = 0;
        int right = number.length - 1;
        int middle = left + (right - left) / 2;
        int index = recursiveBinarySearch(number, left, right, numberForSearch);
        System.out.println(index);
    }


    public static int recursiveBinarySearch(int[] number, int left, int right, int numberForSearch) {

        // условие прекращения
        if (right >= left) {
            int middle = left + (right - left) / 2;

            // если средний элемент - целевой элемент, вернуть его индекс
            if (number[middle] == numberForSearch)
                return middle;

            // если средний элемент больше целевого
            // вызываем метод рекурсивно по суженным данным
            if (number[middle] > numberForSearch)
                return recursiveBinarySearch(number, left, middle - 1, numberForSearch);

            // также, вызываем метод рекурсивно по суженным данным
            return recursiveBinarySearch(number, middle + 1, right, numberForSearch);
        }

        return -1;
    }
}



