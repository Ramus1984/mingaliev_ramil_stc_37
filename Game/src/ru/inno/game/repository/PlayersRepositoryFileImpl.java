package ru.inno.game.repository;


import ru.inno.game.models.Player;

import java.io.*;
import java.util.Map;

public class PlayersRepositoryFileImpl implements PlayersRepository {

    private final String dbPlayersFileName;
    private final String sequencePlayersFileName;
    private Map<String, Player> players;

    public PlayersRepositoryFileImpl(String dbPlayersFileName, String sequencePlayersFileName) {
        this.dbPlayersFileName = dbPlayersFileName;
        this.sequencePlayersFileName = sequencePlayersFileName;
        this.players = players;
    }

    @Override
    public Player findByNickname(String nickname) {
        return null;
    }

    @Override
    public void save(Player player) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(dbPlayersFileName, true));
            player.setId(generateId());
            writer.write(player.getId() + "#" + player.getIp() + "#" + player.getName() + "#" +
                    player.getPoints() + "#" + player.getMaxWinsCount() + "#" + player.getMaxLosesCount() + "\n");
            writer.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Player player) {
        if (players.containsKey(player.getName())) {
            players.put(player.getName(), player);
        } else {
            System.err.println("Такого игрока не существует. Нельзя обновить несуществующего игрока");
        }
    }


    private Long generateId() {
        try (BufferedReader reader = new BufferedReader(new FileReader(sequencePlayersFileName));
             BufferedWriter writer = new BufferedWriter(new FileWriter(sequencePlayersFileName))) {
            // прочитали последний сгенерированный id как строку
            String lastGeneratedIdAsString = reader.readLine();
            // преобразуем ее в Long
            long id = Long.parseLong(lastGeneratedIdAsString);

            writer.write(String.valueOf(id + 1));

            return id;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

}



