package ru.inno.game.repository;

import ru.inno.game.models.Player;

public interface PlayersRepository {

     // метод находит пользователя по никнейму
     Player findByNickname (String nickname);

     // сохраняет нового созданного игрока
     void save (Player player);

     // метод обновления игрока
     void update (Player player);






}

