package ru.inno.game.dto;

import ru.inno.game.models.Game;
import ru.inno.game.models.Player;
import ru.inno.game.repository.GamesRepository;
import ru.inno.game.repository.PlayersRepository;
import ru.inno.game.repository.ShotsRepository;

// информация об игре
public class StatisticDto {

    private Long id;// game ID
    private String namePlayerFirst;
    private String namePlayerSecond;
    private String winner;
    private Integer playerFirstShotsCount; // кол-во выстрелов от первого игрока
    private Integer playerSecondShotsCount; // кол-во выстрелов от второго игрока
    private Integer playerFirstpoints; // кол-во очков
    private Integer playerSecondpoints;
    private Long secondsGameTimeAmount; // продолжительность игры (в секундах)

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamePlayerFirst() {
        return namePlayerFirst;
    }

    public void setNamePlayerFirst(String namePlayerFirst) {
        this.namePlayerFirst = namePlayerFirst;
    }

    public String getNamePlayerSecond() {
        return namePlayerSecond;
    }

    public void setNamePlayerSecond(String namePlayerSecond) {
        this.namePlayerSecond = namePlayerSecond;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public Integer getPlayerFirstShotsCount() {
        return playerFirstShotsCount;
    }

    public void setPlayerFirstShotsCount(Integer playerFirstShotsCount) {
        this.playerFirstShotsCount = playerFirstShotsCount;
    }

    public Integer getPlayerSecondShotsCount() {
        return playerSecondShotsCount;
    }

    public void setPlayerSecondShotsCount(Integer playerSecondShotsCount) {
        this.playerSecondShotsCount = playerSecondShotsCount;
    }

    public Integer getPlayerFirstpoints() {
        return playerFirstpoints;
    }

    public void setPlayerFirstpoints(Integer playerFirstpoints) {
        this.playerFirstpoints = playerFirstpoints;
    }

    public Integer getPlayerSecondpoints() {
        return playerSecondpoints;
    }

    public void setPlayerSecondpoints(Integer playerSecondpoints) {
        this.playerSecondpoints = playerSecondpoints;
    }

    public Long getSecondsGameTimeAmount() {
        return secondsGameTimeAmount;
    }

    public void setSecondsGameTimeAmount(Long secondsGameTimeAmount) {
        this.secondsGameTimeAmount = secondsGameTimeAmount;
    }

    @Override
    public String toString() {
        return  "Игра с ID = " + id + "\n" +
                "Игрок 1: " + namePlayerFirst +  ", попаданий - " + playerFirstShotsCount +  ", всего очков - " + playerFirstpoints + "\n" +
                "Игрок 2: " + namePlayerSecond + ", попаданий - " + playerSecondShotsCount + ", всего очков - " + playerSecondpoints + "\n" +
                "Победа: " + winner + "\n" +
                "Игра длилась: " + secondsGameTimeAmount + " секунд";

    }

}
