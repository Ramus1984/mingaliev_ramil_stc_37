package ru.inno.game.models;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;
import java.util.Timer;

public class Game {

    private Long id;
    private LocalDateTime dateTime; // время проведения игры
    private Player playerFirst; // первый игрок
    private Player playerSecond; // второй игрок
    private Integer playerFirstShotsCount; // кол-во выстрелов от первого игрока
    private Integer playerSecondShotsCount; // кол-во выстрелов от второго игрока
    private Long secondsGameTimeAmount; // продолжительность игры (в секундах)

    public Game(LocalDateTime dateTime, Player playerFirst, Player playerSecond, Integer playerFirstShotsCount, Integer playerSecondShotsCount, Long secondsGameTimeAmount) {
        this.dateTime = dateTime;
        this.playerFirst = playerFirst;
        this.playerSecond = playerSecond;
        this.playerFirstShotsCount = playerFirstShotsCount;
        this.playerSecondShotsCount = playerSecondShotsCount;
        this.secondsGameTimeAmount = secondsGameTimeAmount;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Player getPlayerFirst() {
        return playerFirst;
    }

    public void setPlayerFirst(Player playerFirst) {
        this.playerFirst = playerFirst;
    }

    public Player getPlayerSecond() {
        return playerSecond;
    }

    public void setPlayerSecond(Player playerSecond) {
        this.playerSecond = playerSecond;
    }

    public Integer getPlayerFirstShotsCount() {
        return playerFirstShotsCount;
    }

    public void setPlayerFirstShotsCount(Integer playerFirstShotsCount) {
        this.playerFirstShotsCount = playerFirstShotsCount;
    }

    public Integer getPlayerSecondShotsCount() {
        return playerSecondShotsCount;
    }

    public void setPlayerSecondShotsCount(Integer playerSecondShotsCount) {
        this.playerSecondShotsCount = playerSecondShotsCount;
    }

    public Long getSecondsGameTimeAmount() {
        return secondsGameTimeAmount;
    }

    public void setSecondsGameTimeAmount(Long secondsGameTimeAmount) {
        this.secondsGameTimeAmount = secondsGameTimeAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return Objects.equals(dateTime, game.dateTime) &&
                Objects.equals(playerFirst, game.playerFirst) &&
                Objects.equals(playerSecond, game.playerSecond) &&
                Objects.equals(playerFirstShotsCount, game.playerFirstShotsCount) &&
                Objects.equals(playerSecondShotsCount, game.playerSecondShotsCount) &&
                Objects.equals(secondsGameTimeAmount, game.secondsGameTimeAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dateTime, playerFirst, playerSecond, playerFirstShotsCount, playerSecondShotsCount, secondsGameTimeAmount);
    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", dateTime=" + dateTime +
                ", playerFirst=" + playerFirst.getName() +
                ", playerSecond=" + playerSecond.getName() +
                ", playerFirstShotsCount=" + playerFirstShotsCount +
                ", playerSecondShotsCount=" + playerSecondShotsCount +
                ", secondsGameTimeAmount=" + secondsGameTimeAmount +
                '}';
    }
}


