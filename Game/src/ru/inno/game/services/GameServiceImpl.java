package ru.inno.game.services;


import com.sun.jmx.snmp.SnmpUnknownAccContrModelException;
import ru.inno.game.dto.StatisticDto;
import ru.inno.game.models.Game;
import ru.inno.game.models.Player;
import ru.inno.game.models.Shot;
import ru.inno.game.repository.GamesRepository;
import ru.inno.game.repository.PlayersRepository;
import ru.inno.game.repository.ShotsRepository;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.concurrent.TimeUnit;

// бизнес-логика
public class GameServiceImpl implements GameService{

    private PlayersRepository playersRepository;

    private GamesRepository gamesRepository;

    private ShotsRepository shotsRepository;

    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository,ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname) {
        // получили информацию об обоих игроках, через методы создания и проверки
        Player first = checkIfExists(firstIp, firstPlayerNickname);
        Player second = checkIfExists(secondIp, secondPlayerNickname);


        // создали игру
        Game game = new Game(LocalDateTime.now(),first,second,0,0,0L);
        //и сохранили ее в репозиторий
        gamesRepository.save(game);

        game.setSecondsGameTimeAmount(System.currentTimeMillis());

        // возврат идентификатора игры
        return game.getId();
    }

    private Player checkIfExists(String ip, String nickname) {
        Player player = playersRepository.findByNickname(nickname);

        // если такого игрока нет в репозитории (БД) под таким именем
        if (player == null) {
            // создаем нового игрока
            player = new Player(ip, nickname, 0, 0, 0);
            // и записываем его в репозитории БД
            playersRepository.save(player);
        } else {
            // если такой игрок уже был (есть в репозитории) то обновляем ему IP-адрес
            player.setIp(ip);
            playersRepository.update(player);
        }
        return player;
    }

    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        // получаем того, кто стрелял из репозитория
         Player shooter = playersRepository.findByNickname(shooterNickname);

        // получаем того, в кого стреляли из репозитория
         Player target = playersRepository.findByNickname(targetNickname);

        // получаем игру
         Game game = gamesRepository.findById(gameId);

        // создаем выстрел
         Shot shot = new Shot(LocalDateTime.now(),game,shooter,target);

        // увеличиваем очки у стреляющего
         shooter.setPoints(shooter.getPoints() + 1);

        // если стрелявший - первый игрок
         if (game.getPlayerFirst().getName().equals(shooterNickname)){
             // сохраняем информацию о выстреле в игре
             game.setPlayerFirstShotsCount(game.getPlayerFirstShotsCount()+1);
         }

        // если стрелявший - второй игрок
        if (game.getPlayerSecond().getName().equals(shooterNickname)){
            // сохраняем информацию о выстреле в игре
            game.setPlayerSecondShotsCount(game.getPlayerSecondShotsCount()+1);
        }
        // обновляем данные по стреляющему
         playersRepository.update(shooter);

        // обновляем данные по игре
         gamesRepository.update(game);

        // сохраняем выстрел
         shotsRepository.save(shot);

    }

    @Override
    public StatisticDto finishGame(Long gameId) {
        Game game = gamesRepository.findById(gameId);

        Player player1 = game.getPlayerFirst();

        Player player2 = game.getPlayerSecond();

        StatisticDto statisticDto = new StatisticDto ();

        statisticDto.setId(game.getId());

        statisticDto.setNamePlayerFirst(player1.getName());

        statisticDto.setNamePlayerSecond(player2.getName());

        statisticDto.setPlayerFirstShotsCount(game.getPlayerFirstShotsCount());

        statisticDto.setPlayerSecondShotsCount(game.getPlayerSecondShotsCount());

        statisticDto.setPlayerFirstpoints(game.getPlayerFirstShotsCount());

        statisticDto.setPlayerSecondpoints(game.getPlayerSecondShotsCount());

         if (game.getPlayerFirstShotsCount() < game.getPlayerSecondShotsCount()){
            statisticDto.setWinner(statisticDto.getNamePlayerSecond());
            player2.setMaxWinsCount(player2.getMaxWinsCount() + 1);
            player1.setMaxLosesCount(player1.getMaxLosesCount()+1);

        }else {
             statisticDto.setWinner(statisticDto.getNamePlayerFirst());
             player1.setMaxWinsCount(player1.getMaxWinsCount()+1);
             player2.setMaxLosesCount(player2.getMaxLosesCount()+1);
        }

        long time = System.currentTimeMillis();

        statisticDto.setSecondsGameTimeAmount(TimeUnit.MILLISECONDS.toSeconds (time - game.getSecondsGameTimeAmount()));

        return statisticDto;


    }


}

