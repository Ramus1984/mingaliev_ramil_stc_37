package geometric;

public class Circle extends GeometricFigures implements Scalable {

        public Circle(double rad) {
        if(rad>0.0){
            this.rad = rad;
        }
        perimeterCount();
        areaCount();
    }

    public void perimeterCount () {
        this.perimeter = this.rad * this.pi * 2.0;
    }

    public void areaCount() {
        this.area = this.rad * this.rad * this.pi;
    }

    @Override
    public void scale(double k) {
        this.perimeter *= k;
        this.area = this.area*k*k;
    }
}




