package geometric;

@FunctionalInterface
public interface Scalable {
     void scale (double k);
}





