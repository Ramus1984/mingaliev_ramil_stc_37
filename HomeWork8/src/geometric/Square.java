package geometric;

public class Square extends Parallelogram{
    // квадрат
    public Square(double minLength) {
        super(minLength);
        perimeterCount();
        areaCount();
    }

    public void perimeterCount() {
        this.perimeter = this.minLength * 4;
    }

    public void areaCount() {
        this.area = this.minLength * this.minLength;
    }

    @Override
    public void scale(double k) {
        this.perimeter *= k;
        this.area = this.area*k*k;
    }
}


