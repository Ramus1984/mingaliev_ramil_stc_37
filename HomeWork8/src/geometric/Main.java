package geometric;

public class Main {

    public static void main(String[] args) {

        Rectangle rectangle = new Rectangle(10.0,21.0);
        Square square = new Square(10);
        Circle krug = new Circle(5.0);
        Ellipse ellipse = new Ellipse(10.0,15.0);

        System.out.println("Прямоугольник");

        rectangle.movingCenter(10.0,11.0);

        System.out.println(rectangle.getCoordX() + ":" + rectangle.getCoordY());

        System.out.println(rectangle.getPerimeter());
        System.out.println(rectangle.getArea());

        rectangle.scale(3.0);
        System.out.println(rectangle.getPerimeter());
        System.out.println(rectangle.getArea());


        System.out.println("Квадрат");

        System.out.println(square.getPerimeter());
        System.out.println(square.getArea());
        square.scale(0.5);
        System.out.println(square.getPerimeter());
        System.out.println(square.getArea());


        System.out.println("Круг");

        System.out.println(krug.getArea());
        System.out.println(krug.getPerimeter());
        krug.scale(4.0);
        System.out.println(krug.getArea());
        System.out.println(krug.getPerimeter());

        System.out.println("Эллипс");

        System.out.println(ellipse.getArea());
        System.out.println(ellipse.getPerimeter());
        ellipse.scale(2.5);
        System.out.println(ellipse.getArea());
        System.out.println(ellipse.getPerimeter());


    }
}















