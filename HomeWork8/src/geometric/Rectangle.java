package geometric;

public class Rectangle extends Parallelogram{
    //прямоугольник
    private double maxLength;

    public Rectangle(double minLength, double maxLength) {
        super(minLength);
        if(maxLength>0.0){
            this.maxLength = maxLength;;
        }
        perimeterCount();
        areaCount();

    }

    public void perimeterCount() {
        this.perimeter = (this.minLength + this.maxLength)*2;
    }

    public void areaCount() {
        this.area = this.minLength * this.maxLength;
    }

    @Override
    public void scale(double k) {
        this.perimeter *= k;
        this.area = this.area*k*k;
    }

}



