package geometric;

public class Ellipse extends GeometricFigures implements Scalable {
    //эллипс
    public Ellipse (double minLength,double maxLength) {
        if(minLength>0.0){
            this.minLength = minLength;
        }
        if(maxLength>0.0){
            this.maxLength = maxLength;
        }
        perimeterCount();
        areaCount();
    }

    public void perimeterCount () {
        this.perimeter = 4* (this.pi*this.maxLength*this.minLength + (this.maxLength - this.minLength)*(this.maxLength - this.minLength))/(this.maxLength + this.minLength);
    }

    public void areaCount() {
        this.area = this.minLength * this.pi * this.maxLength;
    }


    @Override
    public void scale(double k) {
        this.perimeter *= k;
        this.area = this.area*k*k;
    }
}



