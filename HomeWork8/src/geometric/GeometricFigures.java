package geometric;

public abstract class GeometricFigures implements Scalable {

    private static final double DEFAULT_MIN_LENGTH = 0.0;
    private static final double DEFAULT_MAX_LENGTH = 0.0;
    private static final double DEFAULT_RADIUS = 0.0;
    private static final double DEFAULT_PI = 3.1415926536;
    private static final double DEFAULT_DIAMETER = 0.0;
    private static final double DEFAULT_X_COORDINATE = 0.0;
    private static final double DEFAULT_Y_COORDINATE = 0.0;
    private static final double DEFAULT_K_COEFFICIENT = 0.0;


    protected double pi = DEFAULT_PI;
    protected double rad = DEFAULT_RADIUS;
    protected double minLength = DEFAULT_MIN_LENGTH;
    protected double maxLength = DEFAULT_MAX_LENGTH;
    protected double coordX = DEFAULT_X_COORDINATE;
    protected double coordY = DEFAULT_Y_COORDINATE;
    protected double k = DEFAULT_K_COEFFICIENT;


    protected double perimeter;
    protected double area;


    public double getPerimeter() {
        return perimeter;
    }

    public double getArea() {
        return area;
    }

    protected abstract void perimeterCount();

    protected  abstract void areaCount();


    protected void movingCenter (double a, double b){
         this.coordX += a;
         this.coordY += b;
    }

    public double getCoordX() {
        return coordX;
    }

    public double getCoordY() {
        return coordY;
    }
}




