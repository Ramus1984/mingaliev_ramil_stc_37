public class Main {

    public static void main(String[] args) {

       NumbersAndStringProcessor numberUtil = new NumbersAndStringProcessor();
//       NumbersAndStringProcessor stringUtil = new NumbersAndStringProcessor();

       NumbersProcess processFunction = number -> {
           int processedNumber =0;
           while (number != 0) {
               processedNumber = processedNumber * 10 + number % 10;
               number /= 10;
           }
           return processedNumber;
       };

//        NumbersProcess processFunction = number -> {
//            if (number != 0) {
//                return Integer.parseInt(Integer.toString(number).replace("0", ""));
//            }
//            return 0;
//        };

//        NumbersProcess processFunction = number -> {
//            int num = Integer.parseInt(Integer.toString(number).
//                    replace("1", "0").
//                    replace("3", "2").
//                    replace("5", "4").
//                    replace("7", "6").
//                    replace("9", "8"));
//            return num;
//        };

//          StringsProcess processFunction = line ->{
//              String reversedStr = new StringBuffer(line).reverse().toString();
//              System.out.println(reversedStr);
//              return reversedStr;
//          };

//        StringsProcess processFunction = line -> {
//            line = line.replaceAll("\\d", "");
//            System.out.println(line);
//            return line;
//        };

//        StringsProcess processFunction = line -> {
//            return line.toUpperCase();
//        };

//        StringsProcess processFunction = line ->{
//            String result = "";
//            for (int i = 0; i < line.length(); i++) {
//                result = line.charAt(i) + result;
//            }
//            return result;
//        };


        numberUtil.process(4021,processFunction);
        numberUtil.process(1406,processFunction);
        numberUtil.process(9070,processFunction);
        numberUtil.process(1001,processFunction);

        numberUtil.showProcessed();


//        stringUtil.process("Hello",processFunction);
//        stringUtil.process("He77ll24f34h134o",processFunction);
//
//        stringUtil.showProcessedString();

    }
}


