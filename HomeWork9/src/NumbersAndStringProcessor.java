public class NumbersAndStringProcessor {


    private static final int MAX_PROCESSED_NUMBERS_COUNT = 10;

    private int [] processedNumbers;

    private String [] proсessedLines;

    private int processedNumberCount;

    public NumbersAndStringProcessor() {
        this.processedNumbers = new int[MAX_PROCESSED_NUMBERS_COUNT];
    }

//    public NumbersAndStringProcessor() {
//        this.proсessedLines = new String[MAX_PROCESSED_NUMBERS_COUNT];
//    }


    public void process (int number,NumbersProcess function){
        if (processedNumberCount < MAX_PROCESSED_NUMBERS_COUNT){
            int processedNumber = function.process(number);
            saveNumber(processedNumber);
        } else{
            System.err.println("Кончилось место для обработки чисел");
        }
    }


    public void process (String line,StringsProcess function){
        if (processedNumberCount < MAX_PROCESSED_NUMBERS_COUNT){
            String proсessedLine = function.process (line);
            saveString (proсessedLine);
        } else{
            System.err.println("Кончилось место для обработки строк");
        }
    }

    private void saveString (String line) {
        proсessedLines[processedNumberCount] = line;
        processedNumberCount++;
    }

    private void saveNumber(int number) {
        processedNumbers[processedNumberCount] = number;
        processedNumberCount++;
    }

    public void showProcessed() {
        for (int i = 0; i < processedNumberCount; i++) {
            System.out.print(processedNumbers[i] +  " ");
        }
    }

    public void showProcessedString() {
        for (int i = 0; i < processedNumberCount; i++) {
            System.out.print(proсessedLines[i] +  " ");
        }
    }

}

