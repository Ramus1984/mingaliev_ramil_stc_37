import java.util.Arrays;
import java.util.Properties;
import java.util.Random;

public class Channel {

      private int channelNumber;
      private String channelName;
      public TV tv;

      public Program [] programs;

      Channel(){

      }

    public Channel(String channelName) {

    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public int getChannelNumber() {
        return channelNumber;
    }

    public void setChannelNumber(int channelNumber) {
        this.channelNumber = channelNumber;
    }

    public Program[] getPrograms() {
        return programs;
    }

    public void setPrograms(Program[] programs) {
        this.programs = programs;
    }

    public void goToChannel(){
        System.out.println("На канале " + getChannelNumber() + " идет передача " + getRandomProgram());
    }

    private Program getRandomProgram(){
          Random random = new Random();
          int randomIndex = random.nextInt(programs.length);
          return programs[randomIndex];
    }

    public Program [] generate (int count){
        String [] showProgram = {
                "Телеканал Доброе утро!",
                "Новости",
                "Телеканал Доброе утро!",
                "Жить здорово!",
                "Модный приговор",
                "Новости (с субтитрами)",
                "Время покажет",
                "Новости (с субтитрами)",
                "Давай поженимся!",
                "Мужское / Женское",
                "Вечерние новости (с субтитрами)",
                "На самом деле",
                "Пусть говорят",
                "Время",
                "Премьера. Курорт цвета хаки: 6 серия т/с",
                "Премьера сезона. Док-ток",
                "Вечерний Ургант"};

        Random random = new Random();
        Program [] programs  = new Program[count];
        for (int i = 0; i < programs.length; i++) {
           programs[i] = new Program(showProgram[random.nextInt(17)]);
        }
        return programs;
    }

/*    public void clickChannelUp(){
        this.channelNumber +=1;
        setChannelNumber(getChannelNumber());
    }

    public void clickChannelDown(){
        this.channelNumber -=1;
        setChannelNumber(getChannelNumber());
    }*/

}
