import java.util.Properties;
import java.util.Random;

public class Program {

    private String programName;

    public Channel channel;

    public Program(String programName) {
        this.programName = programName;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

//    @Override
//    public String toString() {
//        return "Program{" +
//                "programName='" + programName + '\'' +
//                '}';
//    }
    @Override
    public String toString() {
        return programName;
    }
}
