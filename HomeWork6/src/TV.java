public class TV {

     private static final int DEFAULT_CHANNEL_NUMBER = 10;
     private String model;
     private boolean isState;
     private RemoteController remoteController;
     private Channel [] channelNames;
     private int channelNumbersCount = DEFAULT_CHANNEL_NUMBER;
    //    private int [] channelNumbers;

    public TV(String model, int channelNumbersCount) {
        this.model = model;
        this.channelNames = new Channel[DEFAULT_CHANNEL_NUMBER];
        channelToTV(channelNumbersCount);
    }



    private Channel [] channelToTV(int channelNumbersCount) {
        String[] channeltvNames = {
                "Первый канал",
                "2х2",
                "Пятница!",
                "НТВ",
                "Культура",
                "Россия24",
                "Матч ТВ",
                "Discovery",
                "Евроспорт",
                "СТС",
                "РЕН-ТВ"
        };
        Channel [] channelNames = new Channel [channelNumbersCount];
        for (int i = 0; i < channelNames.length; i++) {
            channelNames[i] = new Channel(channeltvNames[i]);
            channelNumbersCount++;
        }
        return channelNames;
    }

    public void setRemoteController (RemoteController remoteController) {
        this.remoteController = remoteController;
    }

    public void powerOn (){
        isState = true;
    }

    public void powerOff (){
        isState = false;
    }

    public boolean isState() {
        return isState;
    }

    public String getModel() {
        return model;
    }


}








