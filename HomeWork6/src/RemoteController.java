public class RemoteController {


    private TV tv;

    public RemoteController(TV tv) {
        this.tv = tv;
    }

    public void controlTv (TV tv) {
        this.tv = tv;
        this.tv.setRemoteController(this);
    }

    public void tvState(){
        if (tv.isState() == false){
            System.out.println("Телевизор " + this.tv.getModel() + " выключен");
        }else{
            System.out.println("Телевизор " + this.tv.getModel() + " включен");
        }
    }

    public void powerOn() {
        tv.powerOn();
    }

    public void powerOff(){
        tv.powerOff();
    }






}

